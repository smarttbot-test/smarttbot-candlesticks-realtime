
Candlesticks Websocket Server
==========================

Para uma melhor experiência na leitura da documentação, utilize a ferramenta online [StackEdit](https://stackedit.io/app#)

Esse projeto realiza a agregação de dados de cotações de criptomoedas em tempo real da API pública Poloniex. 

Requisitos obrigatórios do teste:

* Salvar no banco de dados os candles construídos de periodicidade de 1, 5 e 10 minutos;
* Não utilizar APIs de terceiros para realizar a coleta dos dados na Poloniex. Deve ser desenvolvido o próprio código.
* Gerar candles das criptomoedas Bitcoin e Monero

Requisitos sugeridos do teste:

* Usar o banco de dados MySQL.
* Usar a linguagem Python.
* Utilizar Docker
* Uso de ferramentas open source
* Dar suporte a mais de uma criptomoeda
* Testes unitários
* Git

# Sumário
1. [Estrutura do projeto](#estrutura-projeto)
2. [Instalação](#instalacao)
3. [Execução](#execucao)
4. [Sobre a modelagem do banco de dados](#sobre-o-banco)
5. [Sobre os módulos da aplicação](#sobre-os-modulos) 
6. [Sobre os testes unitários](#sobre-os-testes)
7. [Onde começar a ler o código?](#onde-comecar)
8. [Considerações finais](#consideracoes-finais)


Estrutura do projeto <a id="estrutura-projeto"></a>
-----------------

```
app/
    ├── requirements.txt
    ├── __init__.py
    ├── config.py
    ├── modules
        └── __init__.py
        ├── {module_name}
        │   └── **/__init__.py
        │   ├── **/{filename}_service.py
        │   ├── **/{filename}_repository.py
        │   ├── **/{filename}_exception.py
    └── helpers
        └── __init__.py
        ├── {filename}_helper.py
    └── models
        └── __init__.py
        ├── {filename}_model.py
database/
    └── database_DER.mwb
    ├── database.sql
tasks/
    └── requirements.txt
    ├── __init__.py
    ├── app
        └── __init__.py
        ├── {filename}.py
tests/
    └── requirements.txt
    ├── pytest.ini
    ├── conftest.py
    ├── modules/{module_name}
        └── **/conftest.py
        ├── **/{filename}_test.py
```


#### Pastas:

* `app` - Arquivos de implementação da aplicação.
	* `helpers` - Arquivos auxiliares de uso geral para a aplicação
	* `models` - Contém a representação das entidades do banco de dados ([Peewee](http://docs.peewee-orm.com/en/latest/))
	* `modules` - Módulos da aplicação
* `tasks` - Comandos que auxiliam na execução de ações ([PyInvoke](http://www.pyinvoke.org/)).
* `database` - Arquivos da modelagem da base de dados e o dump da aplicação
* `tests` - Testes da aplicação [pytest](http://pytest.org).

#### Arquivos:

* `README.md`

* `Dockerfile` - Arquivo de configuração Docker que é utilizado para realizar o build da imagem Docker.
* `.dockerignore` - Lista de arquivos que o Docker deve ignorar durante o processo de build.
* `.gitignore` - Lista de arquivos que deve ser ignorado no repositório git.
* `app/config.py` - Arquivo de configuração das variáveis de uso comum em toda a aplicação.
* `app/helpers/{filename}_helper.py` - Classes/funções/variáveis que auxiliam no funcionamento da aplicação.
* `app/models/{filename}_model.py` - Arquivos que representão as entidades no banco de dados.
* `app/modules/**/{filename}_service.py` - Serviços que realizam as execuções das ações dos módulos.
* `app/modules/**/{filename}_repository.py` - Classes intermediárias que auxiliam os serviços a realizarem as operações na base de dados.
* `app/modules/**/{filename}_exception.py` - Contém novas definições de erros para serem tratadas durante a execução da aplicação.
* `**/requirements.txt` - Arquivo com as dependências da aplicação / tests / tasks.
* `app/__init__.py` - Arquivo principal responsável por realizar a criação e a execução da aplicação.


Instalação  <a id="instalacao"></a>
------------

### Banco de dados
Importar o dump da base de dados que está localizado no arquivo [`database/dump.sql`](database/dump.sql).
Esse dump já contém a carga inicial da entidades de acordo com os requisitos do teste.

Atualizar os dados de conexão com o banco de dados nas variáveis de ambiente do arquivo [Dockerfile](Dockerfile)

### Redis
Para facilitar a execução do teste, está configurado um segundo container no docker-compose.yml para utilizar um servidor redis local, porém caso deseje por utilizar seu próprio servidor, basta alterar as variáveis de ambientes do arquivo [Dockerfile](Dockerfile), conforme indicado abaixo:

```
ENV REDIS_PASS=YOUR_REDIS_PASS
ENV REDIS_HOST=YOUR_REDIS_HOST
ENV REDIS_PORT=YOUR_REDIS_PORT
ENV REDIS_DB=YOUR_REDIS_DB # Numérico
```

### Poloniex
Nos testes realizados no websocket da Poloniex, não foi necessário utilizar a chave de API, contudo, caso ocorra algum problema de permissão, base atualizar o arquivo [Dockerfile](Dockerfile) com as seguintes informações:

```
POLONIEX_API_KEY=YOUR_POLONIEX_API_KEY
POLONIEX_SECRET_KEY=YOUR_POLONIEX_SECRET_KEY
```
Caso não possua uma chave de API da Poloniex, acesse [aqui](https://poloniex.com/apiKeys).

Execução <a id="execucao"></a>
------------
### Usando o docker

Para criar a imagem Docker da aplicação e executá-la, basta executar o comando abaixo na raiz do projeto:

```bash
$ docker-compose up --build -d
```

Sobre a modelagem do banco de dados <a id="sobre-o-banco"></a>
--------------------------------
A base de dados foi modelada visando a flexibilização das ações na aplicação. Desta forma, é possível escolher as moedas que devem ser monitoradas e configurar a periodicidade de cada candle.

#### Entidade `exchanger`
Essa entidade foi criada pensando na possibilidade da existência de uma nova API pública. 

#### Entidade `currency`
Contém a lista de todas as moedas disponibilizadas pela permutadora.

#### Entidade `pull_currency`
Lista de quais moedas serão monitoradas.

#### Entidade `frequency`
Definição da periodicidade de cada candle.

#### Entidade `candlestick`
Entidade responsável por armazenar os candles, possuindo as informações da moeda, periodicidade, valores dos candles e datas de seus eventos.


Sobre os módulos da aplicação <a id="sobre-os-modulos"></a>
--------------------------------
Essa aplicação foi desenvolvida utilizando dois módulos, sendo um responsável por realizar a integração da API pública Poloniex e outra responsável por gerenciar os candles, conforme descrito abaixo:

#### Módulo `exchanger/poloniex`

O serviço PoloniexSocketService é responsável por realizar a implementação do websocket da API pública da Poloniex, possuindo como suas principais características:

* Apesar de improvável, essa classe garante que só haverá somente uma instância para cada par de chave `(key, secret)`. Desta forma, o serviço permite a utilização de diferente chaves de API, caso seja necessário;
* Esse serviço permite que seja feita múltiplas inscrições em sua instância. Cada inscrição deve informar um identificador único (utilizado para controle interno no serviço), os IDs das moedas a serem monitoradas e o callback  (funções / métodos) a ser executado quando uma nova mensagem chegar. [`app/modules/exchanger/poloniex/poloniex_service.py:subscribe(id, channel, currencies, callback)`](app/modules/exchanger/poloniex/poloniex_service.py)
* Caso seja necessário remover uma inscrição, basta chamar o método responsável por remover a inscrição, informando somente o identificador da inscrição.
[`app/modules/exchanger/poloniex/poloniex_service.py:unsubscribe(id)`](app/modules/exchanger/poloniex/poloniex_service.py)
* Esse serviço possui um monitoramento nas ações de `subscribe` e `unsubscribe` para iniciar ou parar o websocket, não sendo necessário se preocupar com o gerenciamento dessas ações manualmente;
* Quando uma nova mensagem chega ao websocket, o serviço realiza a verificação da moeda e envia a mensagem para os callbacks inscritos a ela.

Tais ações podem ser exemplificadas abaixo:
```mermaid
graph LR
I((Início))
F((Fim))
P(Poloniex Socket)
M1((Mensagem1))
M2((Mensagem2))
C1[Callback1]
C2[Callback2]
D{Qual moeda?}
I -- Inscreve 'Callback1' para moedas 2 e 3 --> P
I -- Inscreve 'Callback2' para moeda 3 --> P
M1 -- Mensagem da moeda 2 --> P
M2 -- Mensagem da moeda 3 --> P
P --> D
D --> |2| C1
D --> |3| C1
D --> |3| C2
D --> |Desconhecida| F
C1 --> F
C2 --> F
```

#### Módulo `candlestick`

O serviço CandlestickService é responsável por realizar a busca das informações de periodicidade e moedas a serem monitoradas com auxílio do CandlestickRepository

O serviço CandlestickProcessService é responsável por realizar o processamento das mensagens recebidas e gerenciar os candles, conforme descrito abaixo:

* O sistema realiza o gerenciamento das periodicidades para cada moeda, por meio de uma formatação de estilo cron `(*\5 * * * *)`. A biblioteca croniter, gera a próxima data de encerramento do candle, facilitando assim, o gerenciamento dos sticks desses candles;
* Os candles são salvo no redis com tempo de expiração definido de acordo com cada tipo de periodicidade, desta forma, não é necessário gerenciar a limpeza desses cache.
* Quando uma nova data de uma determinada periodicidade é gerada por um novo tick, o serviço realiza a busca no redis do candle encerrado e o salva no banco de dados com auxílio do CandlestickRepository;

Tais ações podem ser exemplificadas abaixo:
```mermaid
graph TB
I((Início))
F((Fim))
P(CandlestickProcessService)
S[Salvar no DB]
G[Gerar periodicidade do candle]
CR[Criar novo candle no Redis]
AR[Atualizar candle no Redis]
D{Candle fechado?}
I -- Recebe mensagem de uma moeda --> P
P --> G
G --> D
D --> |Não| AR
D --> |Sim| S
S --> CR
AR --> F
CR --> F
```

Sobre os testes unitários <a id="sobre-os-testes"></a>
--------------------------------

Os testes unitários estão sendo realizados para os dois módulos da aplicação. 

#### Módulo `exchanger/poloniex`
Os testes estão sendo realizados para garantir que:
* Somente uma instância para cada chave key secret exista
* A inscrição esteja sendo realizada de maneira correta
* A remoção da inscrição esteja sendo realizada de maneira correta
* Que as mensagens serão enviadas para os callbacks correspondentes
* Que as mensagens só sejam enviadas de acordo com as moedas definidas


#### Módulo `candlestick`
Os testes estão sendo realizados para garantir que:
* O candle esteja sendo gerado corretamente
* O candle esteja sendo atualizado quando um novo valor máximo surja
* O candle esteja sendo atualizado quando um novo valor mínimo surja 
* O candle esteja sendo atualizado a cada tick
* Que o candle esteja sendo salvo quando o mesmo encerra.

Onde começar a ler o código? <a id="onde-comecar"></a>
--------------------------------

A aplicação é inicializada por meio do comando `app.run` do PyInvoke, implementado em [`tasks/app/run.py`](tasks/app/run.py):

A tarefa cria a aplicação, de forma a inicializar os componentes necessários para que a aplicação execute propriamente, e a inicializada no arquivo
[`app/__init__.py:create_app()`](app/__init__.py) 


Considerações finais <a id="consideracoes-finais"></a>
--------------------------------

Quando a aplicação inicializa, os primeiros candles podem ser gerados de forma parcial. Está sendo utilizado para auxiliar na geração das datas do candles, a formatação com o estilo tipo cron, um candle de periodicidade do tipo `*\5 * * * *`, que seja inicializado no minuto 47, os sticks só serão analisados entre os minutos 47 até 49:59.

Apesar dos scripts serem flexíveis o suficiente para suportar as configurações de quais moedas a serem processada e quais as suas periodicidade, não foi implementado o recarregamento automático dessas configurações em tempo de execução, sendo assim, necessário a reinicialização do script para buscar as novas configurações.

A base de dados está preparada para realizar somente deleção lógica, desta forma, caso seja necessário deletar algum registro, basta atualizar o campo de data de deleção no registro desejado.

Não é recomendado a edição da periodicidade após candles serem gerados, isso porque podem haver candles diferentes associados a essa mesma periodicidade.

No intuito de facilitar a execução do teste, as variáveis de ambientes estão definidas com seus valores dentro do arquivo Dockerfile, porém é importante ressaltar que essa prática não é recomendada, já que expõem dados sensíveis ao repositório de código, sendo necessário uma nova abordagem de acordo com o método de deploy escolhido.