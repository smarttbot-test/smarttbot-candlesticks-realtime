-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.30-0ubuntu0.18.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for smarttbot_candlesticks_v1
CREATE DATABASE IF NOT EXISTS `smarttbot_candlesticks_v1` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `smarttbot_candlesticks_v1`;

-- Dumping structure for table smarttbot_candlesticks_v1.candlestick
CREATE TABLE IF NOT EXISTS `candlestick` (
  `candlestick_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id do candlestick',
  `frequency_id` tinyint(3) unsigned NOT NULL COMMENT 'Id da frequência',
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Id da moeda',
  `date` datetime NOT NULL COMMENT 'Data do candlestick',
  `open_value` float NOT NULL COMMENT 'Define o valor de abertura do candle no período',
  `open_date` datetime NOT NULL COMMENT 'Data de abertura do candle',
  `close_value` float NOT NULL COMMENT 'Define o valor de fechamento do candle no período',
  `close_date` datetime NOT NULL COMMENT 'Data de encerramento do candle',
  `max_value` float NOT NULL COMMENT 'Define o valor máximo do candle no período',
  `max_date` datetime NOT NULL COMMENT 'Data que o candle atingiu seu valor máximo',
  `min_value` float NOT NULL COMMENT 'Define o valor mínimo do candle naquele período',
  `min_date` datetime NOT NULL COMMENT 'Data que o candle atingiu o seu valor mínimo',
  `type` tinyint(1) unsigned NOT NULL COMMENT 'Tipo de candle (0- Baixa, 1- Alta)',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do registro',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de atualização do registro',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`candlestick_id`),
  KEY `fk_candlestick_frequency_idx` (`frequency_id`),
  KEY `fk_candlestick_currency_idx` (`currency_id`),
  KEY `idx_currency_frequency_date` (`currency_id`,`frequency_id`,`date`),
  KEY `idx_date` (`date`),
  CONSTRAINT `fk_candlestick_currency` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`currency_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_candlestick_frequency` FOREIGN KEY (`frequency_id`) REFERENCES `frequency` (`frequency_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Candlesticks das moedas por periodicidade';

-- Dumping data for table smarttbot_candlesticks_v1.candlestick: ~0 rows (approximately)
/*!40000 ALTER TABLE `candlestick` DISABLE KEYS */;
/*!40000 ALTER TABLE `candlestick` ENABLE KEYS */;

-- Dumping structure for table smarttbot_candlesticks_v1.currency
CREATE TABLE IF NOT EXISTS `currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id da moeda',
  `exchanger_id` tinyint(3) unsigned NOT NULL COMMENT 'Id da permutadora',
  `reference_id` int(10) unsigned NOT NULL COMMENT 'Id de referência fornecido pela da permutadora',
  `name` varchar(45) NOT NULL COMMENT 'Nome da moeda',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de ciação do registro',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de atualização do registro',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`currency_id`),
  KEY `fk_currency_exchanger_idx` (`exchanger_id`),
  KEY `idx_deleted_at` (`deleted_at`),
  CONSTRAINT `fk_currency_exchanger` FOREIGN KEY (`exchanger_id`) REFERENCES `exchanger` (`exchanger_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COMMENT='Moedas disponíveis nas permutadoras';

-- Dumping data for table smarttbot_candlesticks_v1.currency: ~180 rows (approximately)
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT IGNORE INTO `currency` (`currency_id`, `exchanger_id`, `reference_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 336, 'BNB_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(2, 1, 177, 'BTC_ARDR', '2020-06-13 17:03:11', NULL, NULL),
	(3, 1, 253, 'BTC_ATOM', '2020-06-13 17:03:11', NULL, NULL),
	(4, 1, 324, 'BTC_AVA', '2020-06-13 17:03:11', NULL, NULL),
	(5, 1, 210, 'BTC_BAT', '2020-06-13 17:03:11', NULL, NULL),
	(6, 1, 189, 'BTC_BCH', '2020-06-13 17:03:11', NULL, NULL),
	(7, 1, 236, 'BTC_BCHABC', '2020-06-13 17:03:11', NULL, NULL),
	(8, 1, 238, 'BTC_BCHSV', '2020-06-13 17:03:11', NULL, NULL),
	(9, 1, 232, 'BTC_BNT', '2020-06-13 17:03:11', NULL, NULL),
	(10, 1, 14, 'BTC_BTS', '2020-06-13 17:03:11', NULL, NULL),
	(11, 1, 15, 'BTC_BURST', '2020-06-13 17:03:11', NULL, NULL),
	(12, 1, 333, 'BTC_CHR', '2020-06-13 17:03:11', NULL, NULL),
	(13, 1, 194, 'BTC_CVC', '2020-06-13 17:03:11', NULL, NULL),
	(14, 1, 24, 'BTC_DASH', '2020-06-13 17:03:11', NULL, NULL),
	(15, 1, 162, 'BTC_DCR', '2020-06-13 17:03:11', NULL, NULL),
	(16, 1, 27, 'BTC_DOGE', '2020-06-13 17:03:11', NULL, NULL),
	(17, 1, 201, 'BTC_EOS', '2020-06-13 17:03:11', NULL, NULL),
	(18, 1, 171, 'BTC_ETC', '2020-06-13 17:03:11', NULL, NULL),
	(19, 1, 148, 'BTC_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(20, 1, 266, 'BTC_ETHBNT', '2020-06-13 17:03:11', NULL, NULL),
	(21, 1, 246, 'BTC_FOAM', '2020-06-13 17:03:11', NULL, NULL),
	(22, 1, 317, 'BTC_FXC', '2020-06-13 17:03:11', NULL, NULL),
	(23, 1, 198, 'BTC_GAS', '2020-06-13 17:03:11', NULL, NULL),
	(24, 1, 185, 'BTC_GNT', '2020-06-13 17:03:11', NULL, NULL),
	(25, 1, 251, 'BTC_GRIN', '2020-06-13 17:03:11', NULL, NULL),
	(26, 1, 43, 'BTC_HUC', '2020-06-13 17:03:11', NULL, NULL),
	(27, 1, 207, 'BTC_KNC', '2020-06-13 17:03:11', NULL, NULL),
	(28, 1, 275, 'BTC_LINK', '2020-06-13 17:03:11', NULL, NULL),
	(29, 1, 213, 'BTC_LOOM', '2020-06-13 17:03:11', NULL, NULL),
	(30, 1, 250, 'BTC_LPT', '2020-06-13 17:03:11', NULL, NULL),
	(31, 1, 163, 'BTC_LSK', '2020-06-13 17:03:11', NULL, NULL),
	(32, 1, 50, 'BTC_LTC', '2020-06-13 17:03:11', NULL, NULL),
	(33, 1, 229, 'BTC_MANA', '2020-06-13 17:03:11', NULL, NULL),
	(34, 1, 295, 'BTC_MATIC', '2020-06-13 17:03:11', NULL, NULL),
	(35, 1, 342, 'BTC_MDT', '2020-06-13 17:03:11', NULL, NULL),
	(36, 1, 302, 'BTC_MKR', '2020-06-13 17:03:11', NULL, NULL),
	(37, 1, 309, 'BTC_NEO', '2020-06-13 17:03:11', NULL, NULL),
	(38, 1, 64, 'BTC_NMC', '2020-06-13 17:03:11', NULL, NULL),
	(39, 1, 248, 'BTC_NMR', '2020-06-13 17:03:11', NULL, NULL),
	(40, 1, 69, 'BTC_NXT', '2020-06-13 17:03:11', NULL, NULL),
	(41, 1, 196, 'BTC_OMG', '2020-06-13 17:03:11', NULL, NULL),
	(42, 1, 249, 'BTC_POLY', '2020-06-13 17:03:11', NULL, NULL),
	(43, 1, 75, 'BTC_PPC', '2020-06-13 17:03:11', NULL, NULL),
	(44, 1, 221, 'BTC_QTUM', '2020-06-13 17:03:11', NULL, NULL),
	(45, 1, 174, 'BTC_REP', '2020-06-13 17:03:11', NULL, NULL),
	(46, 1, 170, 'BTC_SBD', '2020-06-13 17:03:11', NULL, NULL),
	(47, 1, 150, 'BTC_SC', '2020-06-13 17:03:11', NULL, NULL),
	(48, 1, 204, 'BTC_SNT', '2020-06-13 17:03:11', NULL, NULL),
	(49, 1, 290, 'BTC_SNX', '2020-06-13 17:03:11', NULL, NULL),
	(50, 1, 168, 'BTC_STEEM', '2020-06-13 17:03:11', NULL, NULL),
	(51, 1, 200, 'BTC_STORJ', '2020-06-13 17:03:11', NULL, NULL),
	(52, 1, 89, 'BTC_STR', '2020-06-13 17:03:11', NULL, NULL),
	(53, 1, 182, 'BTC_STRAT', '2020-06-13 17:03:11', NULL, NULL),
	(54, 1, 312, 'BTC_SWFTC', '2020-06-13 17:03:11', NULL, NULL),
	(55, 1, 92, 'BTC_SYS', '2020-06-13 17:03:11', NULL, NULL),
	(56, 1, 263, 'BTC_TRX', '2020-06-13 17:03:11', NULL, NULL),
	(57, 1, 108, 'BTC_XCP', '2020-06-13 17:03:11', NULL, NULL),
	(58, 1, 112, 'BTC_XEM', '2020-06-13 17:03:11', NULL, NULL),
	(59, 1, 114, 'BTC_XMR', '2020-06-13 17:03:11', NULL, NULL),
	(60, 1, 117, 'BTC_XRP', '2020-06-13 17:03:11', NULL, NULL),
	(61, 1, 277, 'BTC_XTZ', '2020-06-13 17:03:11', NULL, NULL),
	(62, 1, 178, 'BTC_ZEC', '2020-06-13 17:03:11', NULL, NULL),
	(63, 1, 192, 'BTC_ZRX', '2020-06-13 17:03:11', NULL, NULL),
	(64, 1, 340, 'BUSD_BNB', '2020-06-13 17:03:11', NULL, NULL),
	(65, 1, 341, 'BUSD_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(66, 1, 306, 'DAI_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(67, 1, 307, 'DAI_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(68, 1, 211, 'ETH_BAT', '2020-06-13 17:03:11', NULL, NULL),
	(69, 1, 190, 'ETH_BCH', '2020-06-13 17:03:11', NULL, NULL),
	(70, 1, 202, 'ETH_EOS', '2020-06-13 17:03:11', NULL, NULL),
	(71, 1, 172, 'ETH_ETC', '2020-06-13 17:03:11', NULL, NULL),
	(72, 1, 176, 'ETH_REP', '2020-06-13 17:03:11', NULL, NULL),
	(73, 1, 179, 'ETH_ZEC', '2020-06-13 17:03:11', NULL, NULL),
	(74, 1, 193, 'ETH_ZRX', '2020-06-13 17:03:11', NULL, NULL),
	(75, 1, 284, 'PAX_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(76, 1, 285, 'PAX_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(77, 1, 326, 'TRX_AVA', '2020-06-13 17:03:11', NULL, NULL),
	(78, 1, 339, 'TRX_BNB', '2020-06-13 17:03:11', NULL, NULL),
	(79, 1, 271, 'TRX_BTT', '2020-06-13 17:03:11', NULL, NULL),
	(80, 1, 335, 'TRX_CHR', '2020-06-13 17:03:11', NULL, NULL),
	(81, 1, 267, 'TRX_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(82, 1, 319, 'TRX_FXC', '2020-06-13 17:03:11', NULL, NULL),
	(83, 1, 316, 'TRX_JST', '2020-06-13 17:03:11', NULL, NULL),
	(84, 1, 276, 'TRX_LINK', '2020-06-13 17:03:11', NULL, NULL),
	(85, 1, 297, 'TRX_MATIC', '2020-06-13 17:03:11', NULL, NULL),
	(86, 1, 344, 'TRX_MDT', '2020-06-13 17:03:11', NULL, NULL),
	(87, 1, 311, 'TRX_NEO', '2020-06-13 17:03:11', NULL, NULL),
	(88, 1, 292, 'TRX_SNX', '2020-06-13 17:03:11', NULL, NULL),
	(89, 1, 274, 'TRX_STEEM', '2020-06-13 17:03:11', NULL, NULL),
	(90, 1, 314, 'TRX_SWFTC', '2020-06-13 17:03:11', NULL, NULL),
	(91, 1, 273, 'TRX_WIN', '2020-06-13 17:03:11', NULL, NULL),
	(92, 1, 268, 'TRX_XRP', '2020-06-13 17:03:11', NULL, NULL),
	(93, 1, 279, 'TRX_XTZ', '2020-06-13 17:03:11', NULL, NULL),
	(94, 1, 254, 'USDC_ATOM', '2020-06-13 17:03:11', NULL, NULL),
	(95, 1, 235, 'USDC_BCH', '2020-06-13 17:03:11', NULL, NULL),
	(96, 1, 237, 'USDC_BCHABC', '2020-06-13 17:03:11', NULL, NULL),
	(97, 1, 239, 'USDC_BCHSV', '2020-06-13 17:03:11', NULL, NULL),
	(98, 1, 224, 'USDC_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(99, 1, 256, 'USDC_DASH', '2020-06-13 17:03:11', NULL, NULL),
	(100, 1, 243, 'USDC_DOGE', '2020-06-13 17:03:11', NULL, NULL),
	(101, 1, 257, 'USDC_EOS', '2020-06-13 17:03:11', NULL, NULL),
	(102, 1, 258, 'USDC_ETC', '2020-06-13 17:03:11', NULL, NULL),
	(103, 1, 225, 'USDC_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(104, 1, 252, 'USDC_GRIN', '2020-06-13 17:03:11', NULL, NULL),
	(105, 1, 244, 'USDC_LTC', '2020-06-13 17:03:11', NULL, NULL),
	(106, 1, 242, 'USDC_STR', '2020-06-13 17:03:11', NULL, NULL),
	(107, 1, 264, 'USDC_TRX', '2020-06-13 17:03:11', NULL, NULL),
	(108, 1, 226, 'USDC_USDT', '2020-06-13 17:03:11', NULL, NULL),
	(109, 1, 241, 'USDC_XMR', '2020-06-13 17:03:11', NULL, NULL),
	(110, 1, 240, 'USDC_XRP', '2020-06-13 17:03:11', NULL, NULL),
	(111, 1, 245, 'USDC_ZEC', '2020-06-13 17:03:11', NULL, NULL),
	(112, 1, 288, 'USDJ_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(113, 1, 323, 'USDJ_BTT', '2020-06-13 17:03:11', NULL, NULL),
	(114, 1, 289, 'USDJ_TRX', '2020-06-13 17:03:11', NULL, NULL),
	(115, 1, 255, 'USDT_ATOM', '2020-06-13 17:03:11', NULL, NULL),
	(116, 1, 325, 'USDT_AVA', '2020-06-13 17:03:11', NULL, NULL),
	(117, 1, 212, 'USDT_BAT', '2020-06-13 17:03:11', NULL, NULL),
	(118, 1, 191, 'USDT_BCH', '2020-06-13 17:03:11', NULL, NULL),
	(119, 1, 260, 'USDT_BCHABC', '2020-06-13 17:03:11', NULL, NULL),
	(120, 1, 298, 'USDT_BCHBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(121, 1, 259, 'USDT_BCHSV', '2020-06-13 17:03:11', NULL, NULL),
	(122, 1, 299, 'USDT_BCHBULL', '2020-06-13 17:03:11', NULL, NULL),
	(123, 1, 320, 'USDT_BCN', '2020-06-13 17:03:11', NULL, NULL),
	(124, 1, 280, 'USDT_BEAR', '2020-06-13 17:03:11', NULL, NULL),
	(125, 1, 337, 'USDT_BNB', '2020-06-13 17:03:11', NULL, NULL),
	(126, 1, 293, 'USDT_BSVBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(127, 1, 294, 'USDT_BSVBULL', '2020-06-13 17:03:11', NULL, NULL),
	(128, 1, 121, 'USDT_BTC', '2020-06-13 17:03:11', NULL, NULL),
	(129, 1, 270, 'USDT_BTT', '2020-06-13 17:03:11', NULL, NULL),
	(130, 1, 281, 'USDT_BULL', '2020-06-13 17:03:11', NULL, NULL),
	(131, 1, 338, 'USDT_BUSD', '2020-06-13 17:03:11', NULL, NULL),
	(132, 1, 304, 'USDT_BVOL', '2020-06-13 17:03:11', NULL, NULL),
	(133, 1, 334, 'USDT_CHR', '2020-06-13 17:03:11', NULL, NULL),
	(134, 1, 308, 'USDT_DAI', '2020-06-13 17:03:11', NULL, NULL),
	(135, 1, 122, 'USDT_DASH', '2020-06-13 17:03:11', NULL, NULL),
	(136, 1, 262, 'USDT_DGB', '2020-06-13 17:03:11', NULL, NULL),
	(137, 1, 216, 'USDT_DOGE', '2020-06-13 17:03:11', NULL, NULL),
	(138, 1, 203, 'USDT_EOS', '2020-06-13 17:03:11', NULL, NULL),
	(139, 1, 330, 'USDT_EOSBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(140, 1, 329, 'USDT_EOSBULL', '2020-06-13 17:03:11', NULL, NULL),
	(141, 1, 173, 'USDT_ETC', '2020-06-13 17:03:11', NULL, NULL),
	(142, 1, 149, 'USDT_ETH', '2020-06-13 17:03:11', NULL, NULL),
	(143, 1, 300, 'USDT_ETHBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(144, 1, 301, 'USDT_ETHBULL', '2020-06-13 17:03:11', NULL, NULL),
	(145, 1, 318, 'USDT_FXC', '2020-06-13 17:03:11', NULL, NULL),
	(146, 1, 217, 'USDT_GNT', '2020-06-13 17:03:11', NULL, NULL),
	(147, 1, 261, 'USDT_GRIN', '2020-06-13 17:03:11', NULL, NULL),
	(148, 1, 305, 'USDT_IBVOL', '2020-06-13 17:03:11', NULL, NULL),
	(149, 1, 315, 'USDT_JST', '2020-06-13 17:03:11', NULL, NULL),
	(150, 1, 322, 'USDT_LINK', '2020-06-13 17:03:11', NULL, NULL),
	(151, 1, 332, 'USDT_LINKBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(152, 1, 331, 'USDT_LINKBULL', '2020-06-13 17:03:11', NULL, NULL),
	(153, 1, 218, 'USDT_LSK', '2020-06-13 17:03:11', NULL, NULL),
	(154, 1, 123, 'USDT_LTC', '2020-06-13 17:03:11', NULL, NULL),
	(155, 1, 231, 'USDT_MANA', '2020-06-13 17:03:11', NULL, NULL),
	(156, 1, 296, 'USDT_MATIC', '2020-06-13 17:03:11', NULL, NULL),
	(157, 1, 343, 'USDT_MDT', '2020-06-13 17:03:11', NULL, NULL),
	(158, 1, 303, 'USDT_MKR', '2020-06-13 17:03:11', NULL, NULL),
	(159, 1, 310, 'USDT_NEO', '2020-06-13 17:03:11', NULL, NULL),
	(160, 1, 124, 'USDT_NXT', '2020-06-13 17:03:11', NULL, NULL),
	(161, 1, 286, 'USDT_PAX', '2020-06-13 17:03:11', NULL, NULL),
	(162, 1, 223, 'USDT_QTUM', '2020-06-13 17:03:11', NULL, NULL),
	(163, 1, 175, 'USDT_REP', '2020-06-13 17:03:11', NULL, NULL),
	(164, 1, 219, 'USDT_SC', '2020-06-13 17:03:11', NULL, NULL),
	(165, 1, 291, 'USDT_SNX', '2020-06-13 17:03:11', NULL, NULL),
	(166, 1, 321, 'USDT_STEEM', '2020-06-13 17:03:11', NULL, NULL),
	(167, 1, 125, 'USDT_STR', '2020-06-13 17:03:11', NULL, NULL),
	(168, 1, 313, 'USDT_SWFTC', '2020-06-13 17:03:11', NULL, NULL),
	(169, 1, 265, 'USDT_TRX', '2020-06-13 17:03:11', NULL, NULL),
	(170, 1, 282, 'USDT_TRXBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(171, 1, 283, 'USDT_TRXBULL', '2020-06-13 17:03:11', NULL, NULL),
	(172, 1, 287, 'USDT_USDJ', '2020-06-13 17:03:11', NULL, NULL),
	(173, 1, 272, 'USDT_WIN', '2020-06-13 17:03:11', NULL, NULL),
	(174, 1, 126, 'USDT_XMR', '2020-06-13 17:03:11', NULL, NULL),
	(175, 1, 127, 'USDT_XRP', '2020-06-13 17:03:11', NULL, NULL),
	(176, 1, 328, 'USDT_XRPBEAR', '2020-06-13 17:03:11', NULL, NULL),
	(177, 1, 327, 'USDT_XRPBULL', '2020-06-13 17:03:11', NULL, NULL),
	(178, 1, 278, 'USDT_XTZ', '2020-06-13 17:03:11', NULL, NULL),
	(179, 1, 180, 'USDT_ZEC', '2020-06-13 17:03:11', NULL, NULL),
	(180, 1, 220, 'USDT_ZRX', '2020-06-13 17:03:11', NULL, NULL);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;

-- Dumping structure for table smarttbot_candlesticks_v1.exchanger
CREATE TABLE IF NOT EXISTS `exchanger` (
  `exchanger_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id da permutadora',
  `name` varchar(45) NOT NULL COMMENT 'Nome da permutadora',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do registro',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de atualização do registro',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`exchanger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Serviços que fornecem transações de criptomoedas';

-- Dumping data for table smarttbot_candlesticks_v1.exchanger: ~0 rows (approximately)
/*!40000 ALTER TABLE `exchanger` DISABLE KEYS */;
INSERT IGNORE INTO `exchanger` (`exchanger_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Poloniex', '2020-06-13 17:01:05', NULL, NULL);
/*!40000 ALTER TABLE `exchanger` ENABLE KEYS */;

-- Dumping structure for table smarttbot_candlesticks_v1.frequency
CREATE TABLE IF NOT EXISTS `frequency` (
  `frequency_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id da frequência',
  `name` varchar(155) NOT NULL COMMENT 'Nome dessa frequência',
  `format` varchar(45) NOT NULL COMMENT 'Definição que informa como o script deve formatar o candlestick',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`frequency_id`),
  UNIQUE KEY `format_UNIQUE` (`format`),
  KEY `idx_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Define a periodicidade que os candlessticks devem ser formados';

-- Dumping data for table smarttbot_candlesticks_v1.frequency: ~3 rows (approximately)
/*!40000 ALTER TABLE `frequency` DISABLE KEYS */;
INSERT IGNORE INTO `frequency` (`frequency_id`, `name`, `format`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '1 minute', '* * * * *', '2020-06-14 19:41:48', '2020-06-14 19:43:30', NULL),
	(2, '5 minutes', '*/5 * * * *', '2020-06-14 19:41:48', '2020-06-15 15:55:21', NULL),
	(3, '10 minutes', '*/10 * * * *', '2020-06-14 19:41:48', '2020-06-15 15:55:23', NULL);
/*!40000 ALTER TABLE `frequency` ENABLE KEYS */;

-- Dumping structure for table smarttbot_candlesticks_v1.pull_currency
CREATE TABLE IF NOT EXISTS `pull_currency` (
  `currency_id` int(10) unsigned NOT NULL COMMENT 'Id da moeda que será extraída',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do registro',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de atualização do registro',
  `deleted_at` datetime DEFAULT NULL COMMENT 'Data de deleção do registro',
  PRIMARY KEY (`currency_id`),
  KEY `fk_pull_currency_currency_idx` (`currency_id`),
  KEY `idx_deleted_at` (`deleted_at`),
  CONSTRAINT `fk_pull_currency_currency1` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`currency_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Define quais moedas devem ser coletadas';

-- Dumping data for table smarttbot_candlesticks_v1.pull_currency: ~2 rows (approximately)
/*!40000 ALTER TABLE `pull_currency` DISABLE KEYS */;
INSERT IGNORE INTO `pull_currency` (`currency_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(128, '2020-06-14 17:11:02', '2020-06-14 17:18:09', NULL),
	(174, '2020-06-14 17:15:07', NULL, NULL);
/*!40000 ALTER TABLE `pull_currency` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
