"""
Auxiliar com o tratamento de datas
"""

from datetime import datetime
from croniter import croniter

class DateHelper:
    """
    Classe responsável tratar datas
    """
    
    _instance = None
    _connections = dict()
    
    def __new__(cls, *args, **kwargs):
        """Metodo singleton."""
        if not cls._instance:
            cls._instance = super(DateHelper, cls).__new__(
                cls, *args, **kwargs)
        return cls._instance
    
    def str_to_date(self, date, format):
        """
        Converte uma string em data
    
        Args:
            date (str): Data em formato de string
            format (int): Formato que se encontra a string
    
        Returns:
            datetime: Data no formato datetime
        """
        datetime_obj = datetime.strptime(date, format)
        
        return datetime_obj
    
    def date_to_str(self, format, date):
        """
        Converte uma data em uma string
    
        Args:
            format (str): Formato que a data deve ser convertido
            date (datetime): Data a ser convertida
    
        Returns:
            str: Data em formato de string
        """
        return date.strftime(format)
    
    def get_diff(self, a, b):
        """
        Calcula a diferença entre duas datas
    
        Args:
            a (datetime): Data a ser comparada a diferença com 'b'
            b (datetime): Data a ser comparada a diferença com 'a'
    
        Returns:
            int: Diferença entre as datas em segundos
        """
        return int((a-b).total_seconds())
    
    def get_current_datetime(self):
        """
        Retornar a data atual
    
        Returns:
            datetime: Data atual
        """
        return datetime.now()
    
    def get_next_from_cron_format(self, format, output_format=None):
        """
        Gera a próxima data baseando-se em um formatdo crontab.
    
        Args:
            format (str): Formato crontab (Ex: * * * * *)
            output_format (str): Define o formato de saída 
                da data
    
        Returns:
            str|datetime: Retorna uma data gerada em formato de 
                string se o for informando o output ou datetime
        """
        iter = croniter(format, datetime.now())
        output = iter.get_next(datetime)
        
        if output_format:
            return output.strftime(output_format)
        
        return output
    
    def get_cron_format_seconds(self, format):
        """
        Retorna o tempo em segundos que um formato cron contem entre
        suas execuções
    
        Args:
            format (str): Formato crontab (Ex: * * * * *)
    
        Returns:
            int: Tempo em segundos do format cron
        """
        iter = croniter(format, datetime.now())
        next = iter.get_next(datetime)
        prev = iter.get_prev(datetime)
        
        return self.get_diff(next, prev)
    
