"""
Auxiliar com o gerenciamento de conexão com o redis
"""

from croniter import croniter
from datetime import datetime
from app import config
import redis

class RedisHelper:
    """
    Classe responsável por criar instâncias redis
    """
    
    _instance = None
    _connections = dict()
    
    def __new__(cls, *args, **kwargs):
        """Metodo singleton."""
        if not cls._instance:
            cls._instance = super(RedisHelper, cls).__new__(
                cls, *args, **kwargs)
        return cls._instance
    
    def connect(self, host=None, port=None, db=None, password=None):
        """
        Connects to redis server
    
        Args:
            host (str): Host da conexão
            port (int): Porta do redis
            db (int): ID do banco
            password (str): Senha do redis
    
        Returns:
            Redis: Redis connection object
        """
        host = host if host else config["REDIS_HOST"]
        port = port if port else config["REDIS_PORT"]
        db = db if db else config["REDIS_DB"]
        password = password if password else config["REDIS_PASS"]
        
        connection_key = host + str(port) + str(db) + str(password) 
        
        if connection_key not in self._connections:
            self._connections[connection_key] = redis.Redis(
                host=host if host else config["REDIS_HOST"],
                port=port if port else config["REDIS_PORT"],
                db=db if db else config["REDIS_DB"],
                password=password if password else config["REDIS_PASS"]
            )
    
        return self._connections[connection_key]
