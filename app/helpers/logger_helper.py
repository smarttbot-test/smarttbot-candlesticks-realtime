"""
Gerencia os logs da aplicação
"""

import logging
from app import config

logger = logging.getLogger(__name__)
logger.setLevel(config["LOG_LEVEL"])