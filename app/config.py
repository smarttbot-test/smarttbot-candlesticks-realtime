# pylint: disable=too-few-public-methods,invalid-name,missing-docstring
import os
import logging

_log_level = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG
}

def _get_log_level(level):
    if level in _log_level:
        return _log_level[level]
    else:
        return _log_level["WARNING"]

config = {
    "PROJECT_ROOT": os.path.abspath(os.path.dirname(__file__)),
    "LOG_LEVEL": _get_log_level(os.environ["LOG_LEVEL"]),
    
    "POLONIEX_API_KEY": os.environ["POLONIEX_API_KEY"],
    "POLONIEX_SECRET_KEY": os.environ["POLONIEX_SECRET_KEY"],
    
    "DB_HOST": os.environ["DB_HOST"],
    "DB_PORT": int(os.environ["DB_PORT"]),
    "DB_NAME": os.environ["DB_NAME"],
    "DB_USER": os.environ["DB_USER"],
    "DB_PASS": os.environ["DB_PASS"],
    
    "REDIS_HOST": os.environ["REDIS_HOST"],
    "REDIS_PORT": int(os.environ["REDIS_PORT"]),
    "REDIS_DB": int(os.environ["REDIS_DB"]),
    "REDIS_PASS": os.environ["REDIS_PASS"],
}
