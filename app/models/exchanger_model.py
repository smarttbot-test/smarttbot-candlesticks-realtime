"""
Modelo que representa a entidade exchanger da base de dados
"""

from peewee import SQL, AutoField, DateTimeField, CharField

from . import BaseModel

class Exchanger(BaseModel):

    exchanger_id = AutoField()

    name = CharField(null=False, max_length=45)
    
    created_at = DateTimeField(null=False, constraints=[
                               SQL('CURRENT_TIMESTAMP')])

    updated_at = DateTimeField(null=True, constraints=[
                               SQL('NULL ON UPDATE CURRENT_TIMESTAMP')])

    deleted_at = DateTimeField(null=True)
