"""
Modelo que representa a entidade currency da base de dados
"""

from peewee import (
    SQL, AutoField, DateTimeField, CharField, IntegerField,
    ForeignKeyField)

from . import BaseModel
from .exchanger_model import Exchanger

class Currency(BaseModel):

    currency_id = AutoField()

    exchanger_id = IntegerField()
    
    exchanger = ForeignKeyField(Exchanger)
    
    reference_id = IntegerField()

    name = CharField(null=False, max_length=45)
    
    created_at = DateTimeField(null=False, constraints=[
                               SQL('CURRENT_TIMESTAMP')])

    updated_at = DateTimeField(null=True, constraints=[
                               SQL('NULL ON UPDATE CURRENT_TIMESTAMP')])

    deleted_at = DateTimeField(null=True)
