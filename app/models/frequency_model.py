"""
Modelo que representa a entidade frequency da base de dados
"""

from peewee import SQL, AutoField, DateTimeField, CharField

from . import BaseModel

class Frequency(BaseModel):

    frequency_id = AutoField()

    name = CharField(null=False, max_length=155)
    
    format = CharField(null=False, max_length=45)
    
    created_at = DateTimeField(null=False, constraints=[
                               SQL('CURRENT_TIMESTAMP')])

    updated_at = DateTimeField(null=True, constraints=[
                               SQL('NULL ON UPDATE CURRENT_TIMESTAMP')])

    deleted_at = DateTimeField(null=True)
