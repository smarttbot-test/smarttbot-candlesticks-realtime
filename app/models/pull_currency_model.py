"""
Modelo que representa a entidade pull_currency da base de dados
"""

from peewee import (
    SQL, AutoField, DateTimeField, CharField, IntegerField,
    ForeignKeyField)

from . import BaseModel
from .currency_model import Currency

class PullCurrency(BaseModel):

    currency_id = IntegerField()
    
    currency = ForeignKeyField(Currency)
    
    created_at = DateTimeField(null=False, constraints=[
                               SQL('CURRENT_TIMESTAMP')])

    updated_at = DateTimeField(null=True, constraints=[
                               SQL('NULL ON UPDATE CURRENT_TIMESTAMP')])

    deleted_at = DateTimeField(null=True)
