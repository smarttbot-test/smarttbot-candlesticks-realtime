"""
Modelo que representa a entidade candlestick da base de dados
"""

from peewee import (
    SQL, AutoField, DateTimeField, IntegerField, ForeignKeyField,
    FloatField)

from . import BaseModel
from .frequency_model import Frequency
from .currency_model import Currency


class Candlestick(BaseModel):

    candlestick_id = AutoField()

    frequency_id = IntegerField()
    
    frequency = ForeignKeyField(Frequency)

    currency_id = IntegerField()
    
    currenty = ForeignKeyField(Currency)
    
    min_value = FloatField(null=False)
    
    min_date = DateTimeField(null=False)
    
    max_value = FloatField(null=False)
    
    max_date = DateTimeField(null=False)
    
    open_value = FloatField(null=False)
    
    open_date = DateTimeField(null=False)
    
    close_value = FloatField(null=False)
    
    close_date = DateTimeField(null=False)
    
    type = IntegerField(null=False)

    date = DateTimeField(null=False)
    
    created_at = DateTimeField(null=False, constraints=[
                               SQL('CURRENT_TIMESTAMP')])

    updated_at = DateTimeField(null=True, constraints=[
                               SQL('NULL ON UPDATE CURRENT_TIMESTAMP')])

    deleted_at = DateTimeField(null=True)
