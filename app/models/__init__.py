"""
Base de todos os modelos de banco de dados.

Responsável por definir a conexão com o banco de dados e alguns
outros demais parâmetros comuns entre os demais modelos.
"""

from peewee import Model, MySQLDatabase
from app import config

db = MySQLDatabase(
    config['DB_NAME'],
    user=config['DB_USER'],
    password=config['DB_PASS'],
    host=config['DB_HOST'],
    port=config['DB_PORT']
)

class BaseModel(Model):
    """
    Essa classe é responsável por definir a conexão com o banco de dados
    e alguns outros demais parâmetros comuns entre os modelos.
    """
    class Meta:
        legacy_table_names = False
        database = db
