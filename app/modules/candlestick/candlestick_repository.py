"""
Intermediador entre os serviços e as entidades para realizar as
oprações de consultas
"""

from app.models.currency_model import Currency
from app.models.exchanger_model import Exchanger
from app.models.pull_currency_model import PullCurrency
from app.models.frequency_model import Frequency
from app.models.candlestick_model import Candlestick


class CandlestickRepository(object):
    def __init__(self):
        """"""
        pass

    def get_pull_currencies(self):
        """
        Lista todas as moedas que devem ser extraídas
        
        Returns:
            list: Lista de moedas
        """
        query = (
            PullCurrency.
            select(
                Currency.currency_id,
                Currency.reference_id
            ).
            join(Currency).
            join(Exchanger).
            where(
                PullCurrency.deleted_at.is_null(),
                Currency.deleted_at.is_null(),
                Exchanger.deleted_at.is_null()
            ).dicts()
        )

        currencies = list(query)

        return currencies
    
    def get_candlesticks_frequency(self):
        """
        Retorna uma lista de periodicidade que os candles devem
        ser gerados
        
        Returns:
            list: Lista de periodicidade
        """
        query = (
            Frequency.
            select(
                Frequency.frequency_id,
                Frequency.format
            ).
            where(
                Frequency.deleted_at.is_null()
            ).dicts()
        )

        frequencies = list(query)

        return frequencies
    
    def save_candlestick(self, candlestick):
        """
        Salva um candlestick
        """
        return Candlestick.create(**candlestick)
