"""
Realiza o processamento dos candles baseado na configurações de periodicidade
de formação do candle e suas moedas.

Essa classe utiliza o redis para fazer o gerenciamento dos candles. Quando um
candle é finalizado o processamento salva esses candles no banco de dados
"""

from .candlestick_repository import CandlestickRepository

class CandlestickProcessService(object):
    """
    Realiza o processamento dos candles
    """
    
    def __init__(self, redis, logger, date_helper, 
                 candlestick_repository, frequencies, pull_currencies):
        """
        Args:
            redis (Redis): Instância redis.
            redis (logging): Logger da aplicação.
            redis (DateHelper): Helper de auxílio de manipulação de datas.
            candlestick_repository (CandlestickRepository): Instância do repositório
                que realiza as operações nas entidades do banco de dados
            frequencies (list): Lista de periodicidade.
            pull_currencies (list): Lista de moedas a serem processadas
        """
        self._redis = redis
        self._frequencies = frequencies
        self._candlestick_repository = candlestick_repository
        self._date_helper = date_helper
        self._logger = logger
        
        self._currencies = dict()
        
        # Define um dicionário reference_id:currency_id para otimizar nos candles
        for currency in pull_currencies:
            self._currencies[currency["reference_id"]] = currency["currency_id"]
            
        self._date_format = "%Y%m%d%H%M"
        self._datetime_str = "%Y-%m-%d %H:%M:%S"
    
    def process_message(self, message):
        """
        Método utilizado para realizar o processamento dos candles.
        Esse métodos é acionado quando uma nova mensagem de um socket ou
        API é recebido
        
        A mensagem é processada para cada periodicidade definida.
        
        Args:
            message (dict): Mensagem a ser processada.
        """
        reference_id = message["id"]
        last_value = message["last"]
        
        try:
            for frequency in self._frequencies:
                candle_date = self._date_helper.get_next_from_cron_format(
                    frequency["format"], self._date_format)
                
                keys = self.generate_chandle_keys(
                    frequency["frequency_id"], reference_id, candle_date)
                
                frequency_currency_key = self._get_currency_frequency_key(
                    frequency["frequency_id"], reference_id)
                
                expiration_key = self._generate_expriration_key(frequency["format"])
                
                self._update_candletickes(keys, candle_date, last_value, expiration_key)
                
                self._save_closed_candle(
                    frequency["frequency_id"], reference_id, 
                    candle_date, frequency_currency_key, expiration_key)
                
        except Exception as e:
            self._logger.critical(e)
            
    def _get_currency_frequency_key(self, frequency_id, reference_id):
        """
        Busca o candlestick no redis
        
        A mensagem é processada para cada periodicidade definida.
        
        Args:
            message (dict): Mensagem a ser processada.
        """
        return "%scr_%sfq" % (reference_id, frequency_id)
            
    def _save_closed_candle(self, frequency_id, reference_id, candle_date, 
                            frequency_currency_key, expiration_key):
        """
        Verifica se o candle foi fechado e sava ele no banco
        
        Args:
            frequency_id (int): ID da frequência
            reference_id (int): ID da moeda
            candle_date (str): Data do candle
        """
        last_candle_date = self._redis.get(frequency_currency_key)
        
        # Verifica se existe algum candle sendo processado e cria se não houver
        if last_candle_date:
            last_candle_date = last_candle_date.decode("utf-8")
        else: 
            last_candle_date = candle_date
            self._redis.set(frequency_currency_key, candle_date, ex=expiration_key)
        
        # Verifica se o candle foi fechado e salva o candle
        if last_candle_date != candle_date:
            
            keys = self.generate_chandle_keys(
                frequency_id, reference_id, last_candle_date)

            # Atualiza a data do último candle no cache
            self._redis.set(frequency_currency_key, candle_date, ex=expiration_key)
            candle_cache = self.get_candlestick(keys["base"] + "*")
            # Verifica se o candle é de alta ou baixa
            if float(candle_cache[keys["last_value"]]) >= float(candle_cache[keys["open_value"]]):
                type = 1
            else:
                type = 0
            
            # Salva o candlestick
            candlestick = {
                "frequency_id": frequency_id,
                "currency_id": self._currencies[reference_id],
                "date": self._date_helper.str_to_date(last_candle_date, self._date_format),
                "open_value": candle_cache[keys["open_value"]],
                "open_date": candle_cache[keys["open_date"]],
                "min_value": candle_cache[keys["min_value"]],
                "min_date": candle_cache[keys["min_date"]],
                "max_value": candle_cache[keys["max_value"]],
                "max_date": candle_cache[keys["max_date"]],
                "close_value": candle_cache[keys["last_value"]],
                "close_date": candle_cache[keys["last_date"]],
                "type": type,
            }
            
            self._candlestick_repository.save_candlestick(candlestick)
            
    
    def generate_chandle_keys(self, frequency_id, reference_id, candle_date):
        """
        Gera as chaves que são utilizada para constrolar os candles no redis
        
        Args:
            frequency_id (int): ID da frequência
            reference_id (int): ID da moeda
            candle_date (str): Data do candle
            
        Returns:
            dict: Chaves do candle
        """
        return {
            "min_value": "%scr_%sfq_%sdt_min_value" % (reference_id, frequency_id, candle_date),
            "min_date": "%scr_%sfq_%sdt_min_date" % (reference_id, frequency_id, candle_date),
            "max_value": "%scr_%sfq_%sdt_max_value" % (reference_id, frequency_id, candle_date),
            "max_date": "%scr_%sfq_%sdt_max_date" % (reference_id, frequency_id, candle_date),
            "open_value": "%scr_%sfq_%sdt_open_value" % (reference_id, frequency_id, candle_date),
            "open_date": "%scr_%sfq_%sdt_open_date" % (reference_id, frequency_id, candle_date),
            "last_value": "%scr_%sfq_%sdt_last_value" % (reference_id, frequency_id, candle_date),
            "last_date": "%scr_%sfq_%sdt_last_date" % (reference_id, frequency_id, candle_date),
            "base": "%scr_%sfq_%sdt" % (reference_id, frequency_id, candle_date)
        }
    
    def _generate_expriration_key(self, format):
        """
        Gera o tempo de expiração das chaves redis com base na periodicidade 
        dos candlestick
        
        Args:
            format (str): Periodicidade em format cron
            
        Returns:
            int: Tempo de expiração
        """
        return self._date_helper.get_cron_format_seconds(format) * 10
    
    def get_candlestick(self, candle_base_key):
        """
        Busca um candestick no redis
        
        Args:
            candle_base_key (str): Chave base dos valores do candlestick no redis
            
        Returns:
            dict: Candlestick
        """
        candlestick = dict()
        candle_keys = self._redis.scan_iter(candle_base_key + "*")
        for key in candle_keys:
            candlestick[key.decode("utf-8")] = (
                self._redis.get(key).decode("utf-8"))
            
        return candlestick
    
    def _update_candletickes(self, keys, candle_date, last_value, expiration_key):
        """
        Atualiza o candle no redis de forma a controlar o valor mínimo, máximo,
        de abertura e fechamento
        
        Args:
            keys (dict): Chaves que são salvas no redis
            candle_date (str): Data do candle
            last_value (float): Último valor do candle
            expiration_key (int): Tempo em segundos de expiração das chaves
        """
        candlestick = self.get_candlestick(keys["base"] + "*")
        with self._redis.pipeline() as pipe:
            current_date = self._date_helper.date_to_str(
                self._datetime_str, self._date_helper.get_current_datetime())

            # Validar se o valor atual é menor do que o mínimo existe
            if (keys["min_value"] not in candlestick 
                    or float(last_value) < float(candlestick[keys["min_value"]])):
                pipe.set(keys["min_value"], last_value).expire(
                    keys["min_value"], expiration_key)
                
                pipe.set(keys["min_date"], current_date).expire(
                    keys["min_date"], expiration_key)
            
            # Validar se o valor atual é maior do que o máximo existe    
            if (keys["max_value"] not in candlestick 
                    or float(last_value) > float(candlestick[keys["max_value"]])):
                pipe.set(keys["max_value"], last_value).expire(
                    keys["max_value"], expiration_key)
                
                pipe.set(keys["max_date"], current_date).expire(
                    keys["max_date"], expiration_key)
            
            # Validar se existe valor de abertura
            if keys["open_value"] not in candlestick:
                pipe.set(keys["open_value"], last_value).expire(
                    keys["open_value"], expiration_key)
                
                pipe.set(keys["open_date"], current_date).expire(
                    keys["open_date"], expiration_key)
            
            # Último valor do candle
            pipe.set(keys["last_value"], last_value).expire(
                keys["last_value"], expiration_key)
            
            pipe.set(keys["last_date"], current_date).expire(
                keys["last_date"], expiration_key)
        
            pipe.execute()
            