"""
Serviço responsável por gerenciar as configurações de candles
"""
from .candlestick_repository import CandlestickRepository


class CandlestickService(object):
    def __init__(self):
        """"""
        pass

    def get_pull_currencies(self):
        """
        Lista todas as moedas que devem ser extraídas
        
        Returns:
            list: Lista de moedas
        """
        candlestick_repository = CandlestickRepository()
        return candlestick_repository.get_pull_currencies()
    
    def get_candlesticks_frequency(self):
        """
        Retorna uma lista de periodicidade que os candles devem
        ser gerados
        
        Returns:
            list: Lista de periodicidade
        """
        candlestick_repository = CandlestickRepository()
        return candlestick_repository.get_candlesticks_frequency()