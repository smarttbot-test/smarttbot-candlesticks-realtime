# encoding: utf-8
"""
Define exceções que podem ocorrer na aplicação poloniex  
"""

class InvalidChannelError(Exception):
    """ Classe de erro para canal inválido """
    pass

class InvalidCallbackError(Exception):
    """ Classe de erro para callback inválido """
    pass

class SubscriberConflictError(Exception):
    """ Classe de erro para id da inscrição duplicada """
    pass
