# encoding: utf-8
"""
Websocket responsável por realizar a coleta de dados de criptomoedas da 
permutadora Poloniex.

Multiplas funções podem se inscrever em um canal para receber as 
mensagens de resposta.

Não é necessário informar ao socket quando iniciar ou parar a escuta
em um canal, o script faz o auto gerenciamento de acordo com as
inscrições realizadas na instância

https://poloniex.com/
"""

import json
import types

from time import time, sleep
from hmac import new
from threading import Thread
from websocket import WebSocketApp
from .poloniex_exception import *
from hashlib import sha512

# python 2
try:
    from urllib import urlencode
# python 3
except:
    from urllib.parse import urlencode

class PoloniexSocketService(object):
    """
    Classe responsável por realizar a extração de dados de criptomoedas
    da Poloniex. Para utilizar a API da Poloniex é necessário criar uma conta
    e realizar a geração de uma chave de API. Para saber mais acesse:
    https://docs.poloniex.com/#introduction
    
    Attributes:
        _instance (dict): Controlador de instâncias por key secret.
    """
    
    _instance = dict()
    
    def __new__(cls, logger, key, secret, *args, **kwargs):
        """
        Metodo que garante que só haverá um instância para cada par key secret.
        
        Args:
            logger (logging): Logger da aplicação.
            key (str): Chave da API.
            secret (str): Segredo da API.
        """
        instance_key = key+secret
        if instance_key not in cls._instance:
            cls._instance[instance_key] = super(PoloniexSocketService, cls).__new__(
                cls, *args, **kwargs)
            cls._instance[instance_key]._init(logger, key, secret, *args, **kwargs)
            
        return cls._instance[instance_key]
    
    def _init(self, logger, key, secret, *args, **kwargs):
        """
        Inicializa os atributos necessários da instância.
        Este método é chamado somente uma vez para cada par key secret quando 
        o método criação executa.
        
        Args:
            logger (logging): Logger da aplicação.
            key (str): Chave da API.
            secret (str): Segredo da API.
        """
        self._socket = WebSocketApp(
            url="wss://api2.poloniex.com/",
            on_open=self._on_open,
            on_message=self._on_message,
            on_error=self._on_error,
            on_close=self._on_close)
        
        self._running = False
        self._logger = logger
        self._thread = None
        
        self._message_cols = [
            "id", "last", "lowest_ask",  "highest_bid",
            "percent_change", "base_volume", "quote_volume",
            "is_frozen", "high_24hr", "low_24hr"
        ]
        
        self._key = key
        self._secret = secret
        
        self._channels = {
            "ticker-data": {
                "id": 1002,
                "listening": False
            }
        }
        
        self._responseChannels = [v["id"] for k,v in self._channels.items()]
        
        self._subscribers = dict()
    
    def _on_open(self, *ws):
        """
        Método responsável por realizar o tratamento de callback de abertura
        do websocket
        
        Args:
            ws: Ponteiro do websocket.
        """
        self._running = True
        self._logger.debug("Poloniex socket opened")
        
    def _on_close(self, *ws):
        """
        Método responsável por realizar o tratamento de callback de fechamento
        do websocket
        
        Args:
            ws: Ponteiro do websocket.
        """
        self._running = False
        self._logger.debug("Poloniex socket closed")

    def _on_message(self, data):
        """
        Valida a mensagem recebida pelo websocket e a envia para os inscritos 
        na intância.
        
        Args:
            data: Mensagem recebida pelo websocket
        """
        message = json.loads(data, parse_float=str)
        
        try:
            id = int(message[0])
        except Exception:
             return self._logger.error("Cannot read message")

        # Captura erros
        if "error" in message:
            return self._logger.error(message["error"])
        
        if id in self._responseChannels and message[1] is None:
            self._send_message_to_subscribers(message, id)
        elif id in self._responseChannels and message[1] is not None:
            if message[1] == 1:
                self._logger.debug("Subscribed to %s", id)
                return False
            
            if message[1] == 0:
                self._logger.debug("Unsubscribed to %s", id)
                return False
            
    def _send_message_to_subscribers(self, message, channel_id):
        """
        Envia uma mensagem de um canal expecífico para os inscritos
        na instância para esse canal.
        
        Args:
            message: Mensagem recebida pelo websocket
            channel_id (int): Id do canal da poloniex
        """
        for subscriber in self._subscribers.values():
            if subscriber["channel_id"] == channel_id:
                currency_id = message[2][0]
                if currency_id in subscriber["currencies"]:
                    formated_message = dict(zip(self._message_cols, message[2]))
                    self._socket._callback(subscriber["callback"], formated_message)

    def _on_error(self, error):
        """
        Método responsável por realizar o tratamento de callback de erro
        do websocket
        
        Args:
            ws: Ponteiro do websocket.
        """
        self._logger.error(error)
        
    def _get_nonce(self):
        """
        Cria um nonce baseado na data corrente
        
        Returns:
            int: Nonce gerado baseado na data corrente.
        """
        return int("{:.6f}".format(time()).replace(".", ""))
    
    def _generate_sign(self):
        """
        Gera a assinatura que é utilizada para increver nos canais
        da API Poloniex
        
        Returns:
            str: Payload encodificado com nonce
            str: hmac_sha512 com o segredo da API key e o payload
        """
        payload = {"nonce": self._get_nonce()}
        payload_encoded = urlencode(payload)
        sign = new(
            self._secret.encode("utf-8"),
            payload_encoded.encode("utf-8"),
            sha512)
        return payload_encoded, sign.hexdigest()

    def subscribe(self, id, channel, currencies, callback):
        """
        Inscreve uma função a um canal da API
        
        Args:
            id (str): Identificador único da inscrição.
            channel (int): Nome do cannal que essa inscrição irá se submeter.
            currencies (list): Lista de moedas a ser monitoradas
            callback: Função ou método que deve ser chamada quando uma mensagem chegar.
        """
        if channel in self._channels:
            if id not in self._subscribers:
                if hasattr(callback, "__call__"):
                    self._subscribers[id] = {
                        "callback": callback,
                        "currencies": currencies,
                        "channel_id": self._channels[channel]["id"]
                    }
                else:
                    raise InvalidCallbackError()
            else:
                raise SubscriberConflictError()
        else:
            raise InvalidChannelError()
        
        self._watch()
        
    def unsubscribe(self, id):
        """
        Remove uma inscrição da instância
        
        Args:
            id (str): Identificador único da inscrição.
        """
        if id in self._subscribers:
            del self._subscribers[id]
        else:
            self._logger.warn("%s has alread unsubscribed", id)
        
        self._watch()

    def _watch(self):
        """
        Monitora as inscrições na instância e gerencia a execução do socket.
        """
        channels = []
        payload, sign = self._generate_sign()
        
        # Verifica se existe algum inscrito e inicializa o socket
        if len(self._subscribers) > 0 and not self._running:
            self._thread = Thread(target=self._socket.run_forever)
            self._thread.start()

            # Espera o socket ser inicializado
            sleep(5)
        
        # Verifica os canais que devem ser escutados
        if self._running:
            for subscriber in self._subscribers.values():
                if subscriber["channel_id"] not in channels:
                    channels.append(subscriber["channel_id"])
    
            # Inicializa ou para a escuta de um canal
            for channel in self._channels.values():
                if channel["id"] in channels and not channel["listening"]:
                    self._socket.send(json.dumps({
                        "command": "subscribe",
                        "channel": channel["id"],
                        "sign": sign,
                        "key": self._key,
                        "payload": payload
                    }))
                elif channel["id"] not in channels and channel["listening"]:
                    self._socket.send(json.dumps({
                        "command": "unsubscribe",
                        "channel": channel["id"],
                        "sign": sign,
                        "key": self._key,
                        "payload": payload
                    }))
        elif len(self._subscribers):
            self._logger.warn(
                "No subscription was made. The socket has not been initialized.")
                
        # Verifica se existe algum inscrito e para o socket se não tiver
        if len(self._subscribers) == 0 and self._running:
            sleep(5)
            try:
                self._socket.close()
            except Exception as e:
                self.logger.exception(e)
            self._thread.join()
            
