# encoding: utf-8
"""
Aplicação responsável por realizar a coleta de dados das criptomoedas e
transformá-las em candles.

As moedas a serem extraídas e processadas são definidas no banco de dados,
assim como também a periodicidade dos candles.
"""

import os
from app.config import config

from app.helpers import RedisHelper, DateHelper, logger

from app.modules.exchanger import PoloniexSocketService
from app.modules.candlestick import (
    CandlestickService, CandlestickProcessService,
    CandlestickRepository)

def create_app(**kwargs):
    """
    Inicializa a aplicação
    """
    
    # Carrega a periodicidade dos candles e as moedas a serem processadas
    candlestick_service = CandlestickService()
    pull_currencies = candlestick_service.get_pull_currencies()
    candlesticks_frequency = candlestick_service.get_candlesticks_frequency()
    
    # Conecta ao redis
    redis = RedisHelper().connect()

    # Inicializa a instância responsável por gerenciar os candles    
    candlestick_process_service = CandlestickProcessService(
        redis, logger, DateHelper(), 
        CandlestickRepository(), candlesticks_frequency, pull_currencies)
    
    # Inicializa o websocket da poloniex
    poloniex = PoloniexSocketService(
        logger,
        config["POLONIEX_API_KEY"], config["POLONIEX_SECRET_KEY"])
    
    currencies = [v["reference_id"] for v in pull_currencies]
    
    # Inscreve o método de processamento de ticks ao websocket
    poloniex.subscribe(
        "smarttbot_candlesticks", 
        "ticker-data", 
        currencies, 
        candlestick_process_service.process_message)
    
    
    