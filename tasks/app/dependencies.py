# encoding: utf-8
"""
Instala as dependências do projeto
"""
import logging
import os
import shutil
import zipfile

try:
    from invoke import ctask as task
except ImportError:  # Invoke 0.13 renomeado de ctask para task
    from invoke import task


log = logging.getLogger(__name__)


@task
def install_python_dependencies(context, force=False):
    """
    Instala as dependências Python listadas em requirements.txt.
    """
    log.info("Instalando dependências...")
    context.run("pip install -r requirements.txt %s" % ("--upgrade" if force else ""))
    log.info("Dependências do projeto instaladas.")

@task
def install(context):
    # pylint: disable=unused-argument
    """
    Instalar dependências do projeto.
    """
    install_python_dependencies(context)
