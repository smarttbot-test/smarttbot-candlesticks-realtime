# encoding: utf-8
"""
Execução da aplicação relacionadas as tarefas de invocação
"""

try:
    from importlib import reload
except ImportError:
    pass
import os
import platform
import warnings

try:
    from invoke import ctask as task
except ImportError:  # Invoke 0.13 renomeado de ctask para task
    from invoke import task


@task(default=True)
def run(
        context,
        install_dependencies=True
    ):
    """
    Executa o servidor.
    """
    if install_dependencies:
        context.invoke_execute(context, "app.dependencies.install")

    from app import create_app
    create_app()
