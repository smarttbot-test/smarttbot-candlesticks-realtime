# encoding: utf-8
"""
Aplicação relacionadas as tarefas de invocação.
"""

from invoke import Collection

from . import dependencies, run

namespace = Collection(
    dependencies,
    run
)

namespace.configure({
    'app': {
    }
})
