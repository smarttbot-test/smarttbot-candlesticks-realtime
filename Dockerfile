FROM python:3.6.6-alpine

ENV API_SERVER_HOME=/opt/src
WORKDIR "$API_SERVER_HOME"
COPY "./requirements.txt" "./"
COPY "./app/requirements.txt" "./app/"
COPY "./tasks" "./tasks"

ENV POLONIEX_API_KEY='YOUR_POLONIEX_API_KEY'
ENV POLONIEX_SECRET_KEY='YOUR_POLONIEX_SECRET_KEY'

ENV DB_HOST='localhost'
ENV DB_PORT=3306
ENV DB_NAME='smarttbot_candlesticks_v1'
ENV DB_USER='root'
ENV DB_PASS='123456'

ENV REDIS_PASS='P3dqmXvhC8ubFW1WxhIoCg4HiZYF7GUJ'
ENV REDIS_HOST='localhost'
ENV REDIS_PORT='6379'
ENV REDIS_DB='0'

ENV LOG_LEVEL='INFO'

RUN apk add --no-cache --virtual=.build_dependencies make musl-dev gcc \
	python3-dev libffi-dev linux-headers c-ares-dev libev-dev && \
    cd $API_SERVER_HOME && \
    pip install -r tasks/requirements.txt && \
    invoke app.dependencies.install && \
    rm -rf ~/.cache/pip && \
    apk del .build_dependencies

COPY "./" "./"

RUN pip install -r tests/requirements.txt && \
	python -m pytest -s -c tests/pytest.ini

USER nobody
CMD [ "invoke", "app.run", "--no-install-dependencies"]
