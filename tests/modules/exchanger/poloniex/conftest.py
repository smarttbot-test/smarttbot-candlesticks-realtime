import pytest
import mock
from app.modules.exchanger import PoloniexSocketService

class PoloniexSocketServiceTest(PoloniexSocketService):
    
    def receive_message(self, message):
        try:
            self._on_message(message)
        except Exception as e:
            self._on_error(e)
            
    def set_running(self, running):
        self._running = running
        
    def set_socket(self, socket):
        self._socket = socket

@pytest.fixture()
def poloniex_socket(logger, default_poloniex_key, default_poloniex_secret):
    poloniex_socket = PoloniexSocketServiceTest(logger, default_poloniex_key, default_poloniex_secret)
    
    socket = mock.MagicMock()
    poloniex_socket.set_socket(socket)
    
    return poloniex_socket

@pytest.fixture()
def poloniex_socket_running(poloniex_socket):
    poloniex_socket.set_running(True)
    return poloniex_socket

@pytest.fixture()
def poloniex_socket_stopped(poloniex_socket):
    poloniex_socket.set_running(False)
    return poloniex_socket

@pytest.fixture(scope="session")
def default_poloniex_key():
    return "key"

@pytest.fixture(scope="session")
def default_poloniex_secret():
    return "secret"
