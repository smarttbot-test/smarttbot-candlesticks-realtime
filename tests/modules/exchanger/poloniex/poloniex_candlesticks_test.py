import pytest
import time
from app.helpers import logger

from app.modules.exchanger import (
    PoloniexSocketService, InvalidChannelError, 
    InvalidCallbackError, SubscriberConflictError)

def default_socket_callback(message):
    return message

def default_poloniex_subscribe(poloniex_socket):
    poloniex_socket.subscribe(
        "candlesticks",
        "ticker-data",
        [1],
        default_socket_callback)

def test_must_not_create_another_instance_with_the_same_secret_key(
        logger, poloniex_socket, default_poloniex_key, default_poloniex_secret):
    poloniex_socket2 = PoloniexSocketService(logger, default_poloniex_key, default_poloniex_secret)
    assert poloniex_socket2 == poloniex_socket
    
def test_must_create_another_instance_with_a_different_secret_key(
        logger, poloniex_socket):
    poloniex_socket2 = PoloniexSocketService(logger, "key2", "secret2")
    assert poloniex_socket2 != poloniex_socket

def test_subscribe_must_work(poloniex_socket_running):
    default_poloniex_subscribe(poloniex_socket_running)
    
def test_duplicated_subscribe_must_fail(poloniex_socket_running):
    with pytest.raises(SubscriberConflictError):
        default_poloniex_subscribe(poloniex_socket_running)
    
def test_unsubscribe_must_work(poloniex_socket_stopped):
    poloniex_socket_stopped.unsubscribe("candlesticks")
    
def test_invalid_channel_must_fail(poloniex_socket_running):
    with pytest.raises(InvalidChannelError):
        poloniex_socket_running.subscribe(
            "candlesticks",
            "invalid-channel",
            [1],
            print)
        
def test_invalid_callback_must_fail(poloniex_socket_running):
    with pytest.raises(InvalidCallbackError):
        poloniex_socket_running.subscribe(
            "candlesticks",
            "ticker-data",
            [1],
            "invalid")

def test_no_message_for_invalid_response(poloniex_socket):
    default_poloniex_subscribe(poloniex_socket)
    resp = None
    try:
        resp = poloniex_socket.receive_message("invalid_message")
    except Exception:
        pass
    assert resp is None
    
def test_no_message_for_heartbeat(poloniex_socket):
    resp = poloniex_socket.receive_message("[1010]")
    assert resp is None
    
def test_no_message_for_poloniex_subscribe_status(poloniex_socket):
    resp = poloniex_socket.receive_message("[1002, 1]")
    assert resp is None

def test_no_message_for_poloniex_subscribe_status(poloniex_socket):
    resp = poloniex_socket.receive_message("""
        [1002,null, [
            149,\"219.42870877\",\"219.85995997\",
            \"219.00000016\",\"0.01830508\",\"1617829.38863451\",
            \"7334.31837942\",0,\"224.44803729\",\"214.87902002\"
        ]]""")
    assert resp is None
