import pytest
import mock
import json
from app.helpers import DateHelper
from app.modules.candlestick import (
    CandlestickRepository, CandlestickProcessService)

last_candlestick_saved = None
candlestick_key = "saved_candlestick"

class CandlestickRepositoryTest(CandlestickRepository):
    
    def __init__(self, redis, date_helper):
        self._redis = redis
        self._date_helper = date_helper
    
    def save_candlestick(self, candlestick):
        candlestick["date"] = None
        self._redis.set(candlestick_key, json.dumps(candlestick))

@pytest.fixture(scope="session")
def candlestick_repository(redis, date_helper):
    return CandlestickRepositoryTest(redis, date_helper)

@pytest.fixture(scope="session")
def saved_candlestick_key(redis):
    return candlestick_key

@pytest.fixture()
def frequency():
    return "* * * * *"

@pytest.fixture(scope="module")
def candlestick_process_service(logger, redis, candlesticks_frequencies, 
                                pull_currencies, date_helper, 
                                candlestick_repository):

    candlestick_process_service = CandlestickProcessService(
        redis, logger, date_helper, 
        candlestick_repository, 
        candlesticks_frequencies, 
        pull_currencies)
    
    return candlestick_process_service