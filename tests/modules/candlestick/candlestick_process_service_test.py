import pytest
import time
import json
from app.helpers import logger
from datetime import datetime, timedelta

from app.modules.candlestick import CandlestickService

def test_candlesticks_creation(redis, date_helper, candlestick_process_service, websocket_message, 
             frequency_id, reference_id, format, output_format):
    
    candlestick_process_service.process_message(websocket_message)
    
    candle_date = date_helper.get_next_from_cron_format(format, output_format)
    keys = candlestick_process_service.generate_chandle_keys(
        frequency_id, reference_id, candle_date)
    
    candlesticks = candlestick_process_service.get_candlestick(keys["base"])
        
    assert (
        float(candlesticks[keys["min_value"]]) == float(websocket_message["last"])
        and float(candlesticks[keys["max_value"]]) == float(websocket_message["last"])
        and float(candlesticks[keys["open_value"]]) == float(websocket_message["last"])
        and float(candlesticks[keys["last_value"]]) == float(websocket_message["last"]))
    
def test_candlesticks_min_value_update(redis, date_helper, candlestick_process_service, websocket_message, 
             frequency_id, reference_id, format, output_format):
    
    websocket_message["last"] = 50
    candlestick_process_service.process_message(websocket_message)
    
    candle_date = date_helper.get_next_from_cron_format(format, output_format)
    keys = candlestick_process_service.generate_chandle_keys(
        frequency_id, reference_id, candle_date)
    
    candlesticks = candlestick_process_service.get_candlestick(keys["base"])
    
    assert float(candlesticks[keys["min_value"]]) < float(candlesticks[keys["open_value"]])
    
def test_candlesticks_max_value_update(redis, date_helper, candlestick_process_service, websocket_message, 
             frequency_id, reference_id, format, output_format):
    
    websocket_message["last"] = 150
    candlestick_process_service.process_message(websocket_message)
    
    candle_date = date_helper.get_next_from_cron_format(format, output_format)
    keys = candlestick_process_service.generate_chandle_keys(
        frequency_id, reference_id, candle_date)
    
    candlesticks = candlestick_process_service.get_candlestick(keys["base"])
    
    assert float(candlesticks[keys["max_value"]]) > float(candlesticks[keys["open_value"]])

def test_close_and_save_candlesticks(redis, date_helper, candlestick_process_service, 
                                     websocket_message, saved_candlestick_key):
    
    websocket_message["last"] = 200
    candlestick_process_service.process_message(websocket_message)

    date_helper.date = datetime.now() + timedelta(hours=3)
    websocket_message["last"] = 250
    candlestick_process_service.process_message(websocket_message)
    
    candlestick = json.loads(redis.get(saved_candlestick_key).decode("utf-8"))

    assert "close_value" in candlestick and float(candlestick["close_value"]) == 200
    
def test_bullish_candlesticks(redis, date_helper, candlestick_process_service, 
                                     websocket_message, saved_candlestick_key):
    
    websocket_message["last"] = 1000
    candlestick_process_service.process_message(websocket_message)

    date_helper.date = datetime.now() + timedelta(hours=3)
    candlestick_process_service.process_message(websocket_message)
    
    candlestick = json.loads(redis.get(saved_candlestick_key).decode("utf-8"))

    assert "close_value" in candlestick and int(candlestick["type"]) == 1

def test_bearish_candlesticks(redis, date_helper, candlestick_process_service, 
                                     websocket_message, saved_candlestick_key):
    
    websocket_message["last"] = 30
    candlestick_process_service.process_message(websocket_message)

    date_helper.date = datetime.now() + timedelta(hours=6)
    candlestick_process_service.process_message(websocket_message)
    
    candlestick = json.loads(redis.get(saved_candlestick_key).decode("utf-8"))
    
    assert "close_value" in candlestick and int(candlestick["type"]) == 0
    
