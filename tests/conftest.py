import pytest
import fakeredis
from app.helpers import logger as loggin
from app.helpers import DateHelper
from datetime import datetime
from croniter import croniter

class DateHelperTest(DateHelper):
    
    date = datetime.now()
    
    def get_next_from_cron_format(self, format, output_format):
        iter = croniter(format, self.date)
        output = iter.get_next(datetime)
        if output_format:
            return output.strftime(output_format)        
        return output
    
    def get_cron_format_seconds(self, format):
        iter = croniter(format, self.date)
        next = iter.get_next(datetime)
        prev = iter.get_prev(datetime)
        return self.get_diff(next, prev)

@pytest.fixture(scope="session")
def date_helper():
    return DateHelperTest()

@pytest.fixture(scope="session")
def logger():
    return loggin

@pytest.fixture(scope="session")
def redis():
    server = fakeredis.FakeServer()
    return fakeredis.FakeStrictRedis(server=server)

@pytest.fixture(scope="session")
def reference_id():
    return 121

@pytest.fixture(scope="session")
def websocket_message(reference_id):
    return {
        "id": reference_id,
        "last": 100
    }
    
@pytest.fixture(scope="session")
def candlesticks_frequencies():
    return [{
        "frequency_id": 1,
        "format": "* * * * *"
    }]
    
@pytest.fixture(scope="session")
def pull_currencies(reference_id):
    return [{
        "currency_id": 1,
        "reference_id": reference_id
    }]

@pytest.fixture(scope="session")
def output_format():
    return "%Y%m%d%H%M"

@pytest.fixture(scope="session")
def frequency_id(candlesticks_frequencies):
    return candlesticks_frequencies[0]["frequency_id"]

@pytest.fixture(scope="session")
def format(candlesticks_frequencies):
    return candlesticks_frequencies[0]["format"]